Metadata Template (File Storage Template)

1. Organization structure of repository

Folder[]>Sub-Folder[]>Description[]>File Type[]

Folder [Data] > Sub-Folder [Raw Data] > Description [Storage location of raw data files. These files may be internal (collected by you or your lab) or external (collected from open sourced platform such as Pangea). No edits should be done in these files directly - they must be preserved in their original format] > Format [.csv]

Folder [Data] > Sub-Folder [Processed Data] > Description [Storage location of processed data files>These are files that contain edits to the original raw data files. They may include tidied data, data with calculations or different data organization structures for use in tables and figures]> Format [.csv]

Folder [Outputs] > Sub-Folder [Tables] > Description [Storage location for summary tables. These include tables that may be used to represent the data in a concise way for use in journal articles or papers. This is athe final output from a given R code and will not be modified any further] > Format [.csv]

Folder [Outputs] > Sub-Folder [Figures] > Description [Storage location for figures that represent the data. These will be used in publication of journal article or thesis chapter] > Format [.png, .jpeg, .img etc.]

Folder [R Code] > Sub-Folder [Temporary] > Description [Storage location for temporary R code. This includes codes that are still being worked on or may need to be modified in the future] > Format [R file]

Folder [R Code] > Sub-Folder [Final] > Description [Storage location for final R code. This includes the final R script that was used to produce the tidied data sets, tables, figures etc. This may be one single file or muliple depending on the complexity of the data] > Format [R file]

Folder [Metadata] > Sub-Folder [N/A] > Description [Storage location for metadata. This section includes data about publication, licensing, location etc.] > Format [.csv]
